---
title: 'Resurrection Remix OS'
taxonomy:
    category:
        - docs
---

# Resurrection Remix OS

Resurrection Remix OS is based on LineageOS with much more settings / features.

Website: [resurrectionremix.com](https://www.resurrectionremix.com/)
