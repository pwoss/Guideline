---
title: 'LineageOS for microG'
taxonomy:
    category:
        - docs
---

# LineageOS for microG

This ROM includes already _[F-Droid](https://guideline.pwoss.xyz/android/projects/f-droid)_ & _[microG](https://guideline.pwoss.xyz/android/projects/microg)_. This makes installation and achieving more privacy very easy!

Website: [lineage.microg.org](https://lineage.microg.org/)
