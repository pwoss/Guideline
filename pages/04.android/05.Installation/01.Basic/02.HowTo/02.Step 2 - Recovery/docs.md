---
title: Step 2 - Recovery
taxonomy:
    category:
        - docs
---

## Reboot to New Recovery
After that, you can reboot your phone to the recovery. Hold __VOL-UP__, __Power Button__ and the __Home Button__ until you reach the new recovery TWRP.

&nbsp;

### WIPE / Delete Internal Storage
Click on __Wipe__ and on __Advanced Wipe__ and select only:
- __Internal Storage__

Now __Swipe to Wipe__.

> Go back to the main view.

&nbsp;

### Backup with TWRP
Before we flash a custom-ROM let's do a "quick" backup of your system.  
Click on __Backup__ and __Select every Partition__ and __Swipe to Backup__.

> Go back to the main view.

&nbsp;

### WIPE / Delete your Phone
Click on __Wipe__ and on __Advanced Wipe__ and select only:
- __Dalvik / ART Cache__
- __System__
- __Data__
- __Cache__

Now __Swipe to Wipe__.

> Go back to the main view.

&nbsp;

## Custom - ROM
Click on __Install__ and __Select Storage__ and choose __Micro SD card__.
Look for _lineage-16.0-20190327-microG-<model-number>.zip_ click on it and __Swipe to confirm Flash__. Wait until it's done. And wipe the _Cache_.

> Go back to the main view.

&nbsp;

## Applications

&nbsp;

### Magisk, NanoDroid, Riru & EdXposed
Click on __Install__ and __Select Storage__ and choose __Micro SD card__.
Look for __Magisk-'latest-version'.zip__, __NanoDroid-BromiteWebView-'latest-version'.zip__, __magisk-riru-core-'latest-version'.zip__ and __magisk-EdXposed-'latest-version'.zip__.
Click at first on __Magisk-'latest-version'.zip__ and then __Add more Zips__ and add the other files.  
Now __Swipe to confirm Flash__.

Go back to the main view and click on __Reboot__ and __System__.

> It may be necessary to start the phone first before installing all these "applications". This means that after installing your custom ROM you will need to boot the system first. Each additional installation of the application must first be started on the system. Install them all separately if you have problems afterwards.
