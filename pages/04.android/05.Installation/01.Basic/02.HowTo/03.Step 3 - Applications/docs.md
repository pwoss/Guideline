---
title: Step 3 - Applications
taxonomy:
    category:
        - docs
---

## Internet Connection
Turn all your internet connection off at first.

&nbsp;

## Install Magisk, EdXposed, XPrivacyLUA & AFWall+ Applications

&nbsp;

### On your Phone
Go to your __Settings__ and click on __About phone__ of your phone. Push the __Build number__ _seven times_ or _more_.
Go __back to__ the main view of the __Settings__ and click on __System__ and __Developer options__ (maybe advanced first).
Turn __Android debugging__ _on_.

Connect your phone via USB to the computer.

&nbsp;

### On your Computer

Start the terminal and go to the folder where you downloaded __... .apk__ files. Maybe /home/user/Download:

__Magisk__
```
adb install MagiskManager-'latest-version'.apk
```
Check your phone for Magisk. Start Magisk and click on the "burger" (three lines top - left) and on modules.
Check if all are selected (Riru - Core, Riru - Ed Xposed). If not do a reboot.

__EdXposed__
```
adb install EdXposedInstaller_'latest-version'.apk && adb install eu.faircode.xlua_'latest-version'.apk && adb install dev.ukanth.ufirewall_'latest-version'.apk
```

Check your phone for EdXposed Installer. Start EdXposed Installer and click on the "burger" (three lines top - left) and on modules.
Select _AFWall+_ and _XPrivacyLUA_.

Do a reboot.
