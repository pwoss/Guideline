---
title: 'Step 1 - Bootloader'
taxonomy:
    category:
        - docs
---

## Backup
If you haven't done it already it's now a good time to do it.
Get all your photos, videos, contacts, calendar, etc. on a separate device.

&nbsp;

## ADB Connection & Unlock Bootloader __(Not for Samsung phones)__

&nbsp;

### ADB
Go to your __Settings__ and click on __About phone__. Push the __Build number__ _seven times_ or _more_.
Go __back to__ the main view of the __Settings__ and click on __System__ and __Developer options__ (maybe advanced first).
Turn __Android debugging__ _on_.

Connect your phone via USB to the computer.

&nbsp;

### Unlock Bootloader
Follow the instruction of the [requirements - bootloader (manufacturer)](https://guideline.pwoss.xyz/android/installation/basic/requirements/software) to unlock your bootloader.
> When you unlock your bootloader, all files on your phone will be deleted!

After that reboot your phone without any new installation (just to save time).

&nbsp;

## TWRP __(Not for Samsung phones)__
This can be necessary again:
Go to your __Settings__ and click on __About phone__ of your phone. Push the __Build number__ _seven times_ or _more_.
Go __back to__ the main view of the __Settings__ and click on __System__ and __Developer options__ (maybe advanced first).
Turn __Android debugging__ _on_.

Connect your phone via USB to the computer again. And go through the following commands:

```
adb reboot bootloader
```
```
fastboot flash recovery your-twrp.img
```
```
adb reboot recovery
```

&nbsp;

## Heimdall - __Samsung phones only__

&nbsp;

### Reboot to Bootloader
Reboot your phone and hold __VOL-DOWN__, __Power Button__ and the __Home Button__ until you see a warning message. Now __VOL-UP__ and you'll see an Android logo and "Downloading ..." etc..  
Connect your phone via USB to your computer.

&nbsp;

### Start Heimdall

#### Device Detection
Start Heimdall and go to __Utilities__. Click on __Detect__ by _Detect Device_. You can see by _Output_ __Device Detected__.

&nbsp;

#### Create .pit file
Now you have to create a .pit (Partition Information Table) file.
Click on __Save as__ by _Download PIT_ and choose a folder and name.

&nbsp;

#### Flash TWRP
Go to __Flash__ and click on __Browse__ by _PIT_. Use the just created .pit file.
Click on __Add__ by _Partitions (files)_ and choose by _Partition Details_ / _Partition Name_ __RECOVERY__. Click on __Browse__ by _File_ and choose __your-twrp.img__.  
Now click on __Start__.

Check if an installation line appears on your phone.

You can also see a process in Heimdall under _Status_. It should look like this:
```
Initialising connection...
Detecting device...
Claiming interface...
Setting up interface...

Initialising protocol...
Protocol initialisation successful.

Beginning session...

Some devices may take up to 2 minutes to respond.
Please be patient!

Session begun.

Downloading device's PIT file...
PIT file download successful.

Uploading RECOVERY
0%
6%
13%
19%
26%
32%
39%
46%
52%
59%
65%
72%
79%
85%
92%
98%
100%

RECOVERY upload successful

Ending session...
Rebooting device...
Releasing device interface...
```

&nbsp;

#### Trouble?
For Linux.
If you encounter errors while trying to download your .pit file, you must create a new file on your computer and add the following:
```
sudo nano /etc/udev/rules.d/79-samsung.rules
```
```
ATTRS{idVendor}=="04e8", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="04e8", ATTRS{product}=="Gadget Serial", ENV{ID_MM_DEVICE_IGNORE}="1", ENV{MTP_NO_PROBE}="1"
```
ctrl + x & yes
```
sudo systemctl restart systemd-udevd
```
Try it again. You may need to restart Heimdall if it's still running.
