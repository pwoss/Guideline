---
title: HowTo
taxonomy:
    category:
        - docs
---

# HowTo

If you got every [requirements](https://guideline.pwoss.xyz/android/installation/basic/requirements) then we can go further with the first step (4 steps in total).

Get yourself a coffee or tea and let's go through this.
