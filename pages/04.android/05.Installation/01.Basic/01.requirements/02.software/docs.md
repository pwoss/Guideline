---
title: Software
taxonomy:
    category:
        - docs
---

### Check Bootloader
Find out if your phone has the ability to unlock your bootloader.  
[xda-developers.com](https://forum.xda-developers.com/) offers a wide selection of phones that will help you find a tutorial on opening your bootloader, if possible.

Straight to your manufacturer:
- [motorola.com](https://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-b)
- [htcdev.com](https://www.htcdev.com/bootloader)
- [sony.com](https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/how-to-unlock-bootloader/#bootloader_guide)
- [lge.com](https://developer.lge.com/resource/mobile/RetrieveBootloader.dev)
- [xiaomi.com](https://account.xiaomi.com/pass/serviceLogin?callback=http%3A%2F%2Fwww.miui.com%2Fextra.php%3Fmod%3Dxiaomi%2Fauthcallback%26followup%3Dhttp%253A%252F%252Fwww.miui.com%252Funlock%252Fapply.php%26sign%3DYzdhOGVjM2ExNDg0YWJlMGUyYTk4NTUwZDY4OGIyZmI5ZmFmZjEzMw%2C%2C&sid=miuibbs&_locale=en)
- *

> Huawei & Honor discontinued support for unlocking the bootloader. There are paid ways out there that I do not like to post here. Sell the phone and buy another one.

> Don't open your bootloader now! Let's do it when we go through the whole installation. Just check it if it's possible or not. If not, you can sell your phone and buy another one. That's the only way, sorry.

&nbsp;

## __Download__

&nbsp;

### ADB & Fastboot Drivers
Depends on your operating system (Linux, Windows, Mac) you have to download the driver for ADB & Fastboot connection (computer to phone).

> Got to [searx.pwoss.xyz](https://searx.pwoss.xyz/) (or whatever you prefer) and search for "adb drivers windows linux and mac". There you can follow a tutorial.

&nbsp;

__Arch Linux & Manjaro Linux:__
```
sudo pacman -S android-tools
```

&nbsp;

### Custom ROM

Download your ROM from [lineage.microg.org](https://download.lineage.microg.org/).  
Get the rom to your phone. Use an external _micro SD card_ if it's possible.

&nbsp;

### TWRP - Recovery

Get the recovery from [twrp.me](https://twrp.me/Devices/)

&nbsp;

### Applications

__Magisk__ from GitHub:  
[Magisk-'latest-version'.zip](https://github.com/topjohnwu/Magisk/releases/)  
[MagiskManager-'latest-version'.apk](https://github.com/topjohnwu/Magisk/releases/)  
[Magisk-uninstaller-<latest-date>.zip](https://github.com/topjohnwu/Magisk/releases/)
> Magisk is necessary to get root access and to install EdXposed & riru.

__Riru__ from GitHub:  
[magisk-riru-core-'latest-version'.zip](https://github.com/RikkaApps/Riru/releases/)
> Riru is necessary for EdXposed.

__EdXposed__ from GitHub:  
[magisk-EdXposed-'latest-version'.zip](https://github.com/ElderDrivers/EdXposed/releases/)  
[EdXposedInstaller_'latest-version'.apk](https://github.com/ElderDrivers/EdXposed/releases/)  
[EdXposedUninstaller_rec.zip](https://github.com/ElderDrivers/EdXposed/releases/)
> EdXposed is necessary for XPrivacyLua

__NanoDroid__ from nanolx.org:  
[NanoDroid-BromiteWebView-<latest-date>.zip](https://downloads.nanolx.org/NanoDroid/Stable/)

__XPrivacyLua__ from F-Droid:  
[eu.faircode.xlua_'latest-version'.apk](https://f-droid.org/en/packages/eu.faircode.xlua/)
> Scroll down to 'Download APK'

__AFWall+__ from F-Droid:  
[dev.ukanth.ufirewall_'latest-version'.apk](https://f-droid.org/en/packages/dev.ukanth.ufirewall/)
> Scroll down to 'Download APK'

Get all downloaded applications on your phone. Use an external _micro SD card_ if it's possible.  
Leave __eu.faircode.xlua_'latest-version'.apk__, __dev.ukanth.ufirewall_'latest-version'.apk__, __MagiskManager-'latest-version'.apk__ and __EdXposedInstaller-'latest-version'.apk__ on your computer.

&nbsp;

### Heimdall - Samsung phones only

Download _Heimdall_ from [glassechidna.com.au](https://glassechidna.com.au/heimdall/)
> This is necessary to install TWRP.

__Arch Linux & Manjaro Linux:__
```
pikaur -S heimdall
```
