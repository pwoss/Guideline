---
title: Requirements
taxonomy:
    category:
        - docs
---

# Requirements

This is a little bit tricky! It can be different depends on your phone.
Like we wrote in the beginning, "_The main tutorial is based on a Samsung Galaxy Note 4 (2014), however the same steps may be replicated in a similar way for other devices._"  

> If you are completely unsure how all this works just send us an email or join the [community](https://forum.pwoss.xyz/).
