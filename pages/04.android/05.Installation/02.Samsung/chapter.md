---
title: Samsung
taxonomy:
    category:
        - docs
---

# Samsung

Samsung has quite a few good phones out there. And a lot of official LineageOS ROMs are available. But Samsung is also very annoying. They have so many different kinds of the same phone which is not really obvious.  
The __Galaxy Note 4__ for example has __25 different kinds__. That's the reason, you have to find out the model number first before you download / flash any Custom-ROMs, TWRP etc.  

Some has another modem, two sim slots or whatever, but the main different can be the CPU. Qualcomms Snapdragon [Wikipedia](https://en.wikipedia.org/wiki/Qualcomm_Snapdragon) or Samsungs Exynos [Wikipedia](https://en.wikipedia.org/wiki/Exynos).  
The Snapdragon CPU has a very good support (Custom-ROMs). We suggest to try to get a Snapdragon CPU.
