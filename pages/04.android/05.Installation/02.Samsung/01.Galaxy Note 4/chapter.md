---
title: Galaxy Note 4
taxonomy:
    category:
        - docs
---

# Galaxy Note 4

Released, October 2014.

The Galaxy Note 4 is still a very good phone. The problem with it it has no official release of _LineageOS_ and so no _LineageOS for microG_. But it is still possible to use another Custom-ROM. We'll go with _Resurrection Remix_ in this tutorial which is official. 
