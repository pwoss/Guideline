---
title: Software
taxonomy:
    category:
        - docs
---


## __Download__

&nbsp;

### Custom ROM

Download your ROM from [resurrectionremix.com](https://get.resurrectionremix.com/?dir=trlte).  
Get the rom to your phone. Use an external _micro SD card_ if it's possible.

&nbsp;

### TWRP - Recovery

Get the recovery from [twrp.me](https://twrp.me/samsung/samsunggalaxynote4qualcomm.html)

&nbsp;

### ADB Drivers
> Got to [searx.pwoss.xyz](https://searx.pwoss.xyz/) (or whatever you prefer) and search for "adb drivers windows linux and mac". You can follow a tutorial there.

&nbsp;

__Arch Linux & Manjaro Linux:__
```
sudo pacman -S android-tools
```

&nbsp;

### Heimdall

Download _Heimdall_ from [glassechidna.com.au](https://glassechidna.com.au/heimdall/)
> This is necessary to install TWRP.

__Arch Linux & Manjaro Linux:__
```
pikaur -S heimdall
```

&nbsp;

### Applications

__Magisk__ from GitHub:  
[Magisk-'latest-version'.zip](https://github.com/topjohnwu/Magisk/releases/)  
[MagiskManager-'latest-version'.apk](https://github.com/topjohnwu/Magisk/releases/)  
[Magisk-uninstaller-'latest-version'.zip](https://github.com/topjohnwu/Magisk/releases/)
> Magisk is necessary to get root access and to install EdXposed & riru.

__Riru__ from GitHub:  
[magisk-riru-core-'latest-version'.zip](https://github.com/RikkaApps/Riru/releases/)
> Riru is necessary for EdXposed.

__EdXposed__ from GitHub:  
[magisk-EdXposed-'latest-version'.zip](https://github.com/ElderDrivers/EdXposed/releases/)  
[EdXposedInstaller_'latest-version'.apk](https://github.com/ElderDrivers/EdXposed/releases/)  
[EdXposedUninstaller_rec.zip](https://github.com/ElderDrivers/EdXposed/releases/)
> EdXposed is necessary for XPrivacyLua

__NanoDroid__ from nanolx.org:  
[NanoDroid-BromiteWebView-'latest-version'.zip](https://downloads.nanolx.org/NanoDroid/Stable/)

__XPrivacyLua__ from F-Droid:  
[eu.faircode.xlua_'latest-version'.apk](https://f-droid.org/en/packages/eu.faircode.xlua/)
> Scroll down to 'Download APK'

__AFWall+__ from F-Droid:  
[dev.ukanth.ufirewall_'latest-version'.apk](https://f-droid.org/en/packages/dev.ukanth.ufirewall/)
> Scroll down to 'Download APK'

__FakeGapps__ from F-Droid:  
[com.thermatk.android.xf.fakegapps_'latest-version'.apk](https://f-droid.org/en/packages/com.thermatk.android.xf.fakegapps/)
> Scroll down to 'Download APK'

Get all downloaded applications on your phone. Use an external _micro SD card_ if it's possible.  
Leave __eu.faircode.xlua_'latest-version'.apk__, __dev.ukanth.ufirewall_'latest-version'.apk__, __MagiskManager-'latest-version'.apk__, __com.thermatk.android.xf.fakegapps-'latest-version'.apk__ and __EdXposedInstaller-'latest-version'.apk__ on your computer.
