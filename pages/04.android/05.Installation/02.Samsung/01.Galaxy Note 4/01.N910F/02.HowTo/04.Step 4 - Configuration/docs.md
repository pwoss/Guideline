---
title: Step 4 - Configuration
taxonomy:
    category:
        - docs
---

## Internet Connection
Turn your Internet on (WIFI or / and LTE).

&nbsp;

## AFWall+
Start AFWall+ and click on the _three dots_ (top-right) and click on __Preferences__ and on __UI Preferences__

&nbsp;

### UI Preferences
Select:
- Enable Notifications
- Show notification icon
- Rules Progress
- Show UID for apps
- Confirm AFWall+ disable

Go back and click on __Rules/Connectivity__.

&nbsp;

### Rules/Connectivity
Select:
- Active rules
- Roaming control
- LAN control
- VPN control

> Check IPv6 Chains if you are using IPv6.

Go back to the main view (first start of the app).

&nbsp;

### Allow Internet Access for certain Apps
Select _WLAN, Mobile Connection (2 arrows without roaming (R)) and VPN_ for:
- [1000] Android System, Advanced, ...
- [10008] Media Storage, Download ...
- Android System Web View
- Browser
- F-Droid
- Magisk Manager
- microG DroidGuard Helper
- microG Service Core
- *

Depending on your application, you may need to give your application access to the Internet when needed. Each time you install an application, a notification is displayed, and you can choose whether this application needs Internet or not. If no pop-up notification is displayed, it may be because the AFWall + message is not working properly or your installed application does not require Internet access.

&nbsp;

## microG

&nbsp;

### Spoof package signature
Go to your phone's settings and click on __Apps & notifications__, __Advanced__ and __App permission__.
Now click __Spoof package signature__ and click on the _three dots_ (top-right) and on __Show system__ and select _FakeStore_ and _microG Services Core_.

&nbsp;

### Permissions
Start the microG application and grant all necessary permissions. Then click on __Self-Check__ and check everything. It should have everything selected. Maybe _UnifiedNlp status_ did not select everything. That's okay.

&nbsp;

### Google device registration & Cloud Messaging
Go back to _microG Settings_ and click __Google Device Registration__ and enable it.
The same applies to __Google Cloud Messaging__.

> If you do not install apps from the Play Store via Yalp, you do not need to enable Google.  
> If you're using _AdAway_ or other ADS blocking apps on your phone you'll have to add to your _Whitelist_ = __mtalk.google.com__ if you need Google.

&nbsp;

## XPrivayLUA
This is quite similar like AFWall+.
Start _XPrivacyLUA_ and click on the "burger" (three lines top - left) and select __Notify on new apps__ and __Restrict new apps__.

Example:
- Contact Apps need the possibility to read you contacts. You have to unselect __Get contacts__.
- GPS need access to your Location. You have to unselect __Get location__.

It's actually pretty obvious.

XPrivacyLUA doesn't block the contacts for example. It fakes it.
If you block __Read clipboard__ and you paste a name to your _Contact app_ it doesn't paste the name. It paste _Private_ instead.

Don't forget that.

That's it. Enjoy
