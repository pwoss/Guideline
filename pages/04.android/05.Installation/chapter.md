---
title: Installation
taxonomy:
    category:
        - docs
---

# Installation

There is a basic installation for all phones, but based on _Samsung Galaxy Note 4_. The installation is slightly different than other brands. However, we may be able to add more brands in the future.

> If you want to add your phone-installation, click the "Edit this page" button and start a pull request on GitHub or send us an email or join the [community](https://forum.pwoss.xyz/).
