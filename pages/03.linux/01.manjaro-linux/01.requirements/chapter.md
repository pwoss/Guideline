---
title: Requirements
taxonomy:
    category:
        - docs
---

# Requirements - Manjaro Linux
To get your new operating system on your PC you'll need a few things:

Hardware, Software and a little of your time.
