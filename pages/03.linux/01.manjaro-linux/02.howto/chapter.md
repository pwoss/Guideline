---
title: HowTo
taxonomy:
    category:
        - docs
---

# Manjaro Linux - HowTo

The easiest way is to follow the instruction of Manjaro itself.
[manjaro.org](https://osdn.net/projects/manjaro/storage/Manjaro-User-Guide.pdf/)
