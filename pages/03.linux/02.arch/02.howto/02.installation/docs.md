---
title: Installation
taxonomy:
    category:
        - docs
---

### Flash the ISO file
```
fdisk -l
```
or with _sudo_

```
sudo fdisk -l
```
(check _of=/dev/sdd_, if it's really your USB Stick!!!)

```
sudo dd bs=4M if=~/Downloads/archlinux-<version>-x86_64.iso of=/dev/sdd
```

> You can also use [Etcher](https://www.balena.io/etcher/) if you prefer a graphical user interface (GUI).
