---
title: Hardware
taxonomy:
    category:
        - docs
---

# Arch Linux

### Minimal System Requirements
> - 512 megabyte (MB) of memory (RAM)
> - 800 megabytes (MB) of hard disk space
> - A one gigahertz (GHz) processor
> - A high definition (HD) graphics card and monitor
> - A broadband internet connection
> - x86_64-compatible machine

&nbsp;

### Recommended System Requirements
> - 4 gigabyte (GB) of memory (RAM)
> - 40 gigabytes (GB) of hard disk space
> - A 4 gigahertz (GHz) processor
> - A high definition (HD) graphics card and monitor
> - A broadband internet connection
> - x86_64-compatible machine

&nbsp;

### System Architecture

The following items are also recommended:
- A reliable 1GB (or greater) USB stick

The USB Stick is necessary for the installation of the system. Also, having a reliable USB stick from a reputable brand will help ensure that the process goes smoothly.
