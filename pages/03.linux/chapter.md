 ---
title: Linux
taxonomy:
    category:
        - docs
---

# Manjaro Linux & Arch Linux

If you no longer want to use Windows, Mac, etc., here's how to install two alternative operating systems based on Linux (open source).  

Where _Arch Linux_ can not really be recommended, because the installation can take a lot of time. Proper installation of your hardware (drivers) can be difficult and can cause problems, but it's perfect to learn more about Linux / Computer. And if you run into any problems we can help you.  

But the easiest way is definitely the installation of Manjaro Linux.
