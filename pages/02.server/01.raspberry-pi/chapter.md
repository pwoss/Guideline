---
title: 'Raspberry Pi'
taxonomy:
    category: docs
---

# Raspberry Pi

The Raspberry Pi is the most popular single-board computer.

It's perfectly made (amongst other things) to get a running server at your home. It doesn't cost much power and it's small so it fits everywhere.

Our PwOSS - Image (operating system) is for the __Pi 2,3__ and __3B+__.
