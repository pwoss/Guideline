---
title: 'Step 3'
taxonomy:
    category:
        - docs
---

### Create an no-ip account
Follow the link [www.noip.com](https://www.noip.com/sign-up) and create an account and copy your chosen hostname.  
You’ll need it for the installation of the server.

&nbsp;

### The .md file
Go through the ['(Image Docu).md'](https://seafile.pwoss.xyz/d/24060481bb8e4a6aa4cf/?p=/1.%20Raspberry%20Pi/Arch%20Linux%20%7C%20ARM/PwOSS%20-%20Image/Latest&mode=list) file. Just copy and paste.

> It has been tested many times, but something can go wrong... you know, ask.
