---
title: 'Step 2'
taxonomy:
    category:
        - docs
---

### Set up your Pi
[Raspberrypi.org](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/4) (Connect your external hard drive as well)

Turn on your Pi.

&nbsp;

### How to connect to your Raspberry Pi
Open your terminal on your PC.

> - Linux – Search your apps for your terminal  
> - Mac – Search your apps for your terminal  
> - Windows – [Raspberrypi.org](https://www.raspberrypi.org/documentation/remote-access/ssh/windows.md)  
You have to find your local IP address of your Raspberry Pi to connect per SSH to the terminal.

Terminal command:
```
arp -n | awk '/b8:27:eb/ {print $1}'
```

In my constellation, it's 192.168.1.76. You can find it on your router as well.

Open your terminal and type:
```
ssh pwoss@192.168.1.76
pwoss
```

> If you’re fully helpless, just ask us or the [forum](http://forum.pwoss.xyz/).
