---
title: 'Step 4'
taxonomy:
    category:
        - docs
---

### Reboot Server
Done? Did you restart your server? Everything seems to be fine?  
Good!

&nbsp;

### Port Forwarding
You’ll need the port forwarded to your Raspberry Pi - IP (192.168.1.76 <- can be this one).

The 1194 (udp) port needs to be open in your router for the [VPN connection](https://en.wikipedia.org/wiki/OpenVPN) (Wikipedia link).  

> [https://www.noip.com](https://www.noip.com/support/knowledgebase/general-port-forwarding-guide/) has a good howto of some router brands.  
> Your router isn’t listed? Just ask us or the [forum](https://forum.pwoss.xyz/).

&nbsp;

### Primary DNS Server
Last step will be to change the DNS server. This is necessary to get every device through Pi-Hole.
Login in to your router and change the "primary DNS server" under "DHCP-Server".
> This can be named differently. Depends on your router.

Delete the "secondary DNS server" and save it.
> Might be necessary to re-login all your connected devices to your WIFI/LAN.

That's it.
