---
title: Software
taxonomy:
    category:
        - docs
---

# Arch Linux

&nbsp;

## __Download__

&nbsp;

### Arch - ISO

Download the .iso file and the .iso.sig file from some of the listed provider from  [archlinux.org](https://www.archlinux.org/download/) and open the md5.txt file.
> Arch Linux is only available for 64-bit systems.

&nbsp;

Check the two files in the same folder with the following command/s:

- for Arch user
  - ```pacman-key -v archlinux-<version>-x86_64.iso.sig```
- other [GnuPGP](https://wiki.archlinux.org/index.php/GnuPG) systems
  - ```gpg --keyserver pgp.mit.edu --keyserver-options auto-key-retrieve --verify archlinux-<version>-x86_64.iso.sig```
- and check the md5sum with the following command
  - ```md5sum archlinux-<version>-x86_64.iso```

> Another method to verify the authenticity of the signature is to ensure that the public key's fingerprint is identical to the key fingerprint of the [Arch Linux developer](https://www.archlinux.org/people/developers/) who signed the ISO-file. See [Wikipedia:Public-key_cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) for more information on the public-key process to authenticate keys.

&nbsp;

### (Optional) Etcher

Download Etcher to flash the ISO
  - [Etcher](https://www.balena.io/etcher/) (www.balena.io - link)

&nbsp;

### (Scratch Docu).md
You can download or follow the .md file from [seafile.pwoss.xyz](https://seafile.pwoss.xyz/d/24060481bb8e4a6aa4cf/?p=/3.%20Arch%20Linux%20-%2064-bit/Arch%20Linux%20-%20%28Scratch%20Docu%29&mode=list) or from [GitHub](https://github.com/PwOSS/Documentation/tree/master/Arch%2064-bit).
