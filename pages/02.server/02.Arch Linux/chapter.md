---
title: 'Arch Linux 64-bit'
taxonomy:
    category: docs
---

# Arch Linux

A 64-bit server offers much more power than a Raspberry Pi. However, the cost of power (electricity) and hardware could be more than a pi.

> There is no image/iso for now. We might be create a bootable USB stick or/and a .vdi (VirtualBox) file. You have to use the guide & scratch combo for now.
