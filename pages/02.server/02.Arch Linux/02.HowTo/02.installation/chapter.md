---
title: Installation
taxonomy:
    category:
        - docs
---

# Installation

If you got every [requirements](https://guideline.pwoss.xyz/server/arch%20linux/requirements) then we can go further with the first step (3 steps in total).

Get yourself a coffee or tea and let's go through this.
