---
title: HowTo
taxonomy:
    category:
        - docs
---

# Arch Linux - HowTo
If you got every [requirements](https://guideline.pwoss.xyz/server/arch%20linux/requirements) then we can go further with the first step.

Get yourself a coffee or tea and let's go through this.
