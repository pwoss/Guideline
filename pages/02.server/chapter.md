---
title: Server
taxonomy:
    category:
        - docs
---
# Server

&nbsp;

### Raspberry Pi
1. Our PwOSS - Image and the documentation "(Image Docu).md" file = For the Raspberry Pi 2,3 and 3B+.
2. Arch Linux | ARM - Image. Start from the beginning with our "(Scratch Docu).md" file and the image from [archlinuxarm.org](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)

&nbsp;

### Arch Linux - 64 bit
This is only a "(Scratch Docu).md" file as well.
We're might be able one day to get our own live .ISO file or a .VDI (Virtual Box) file.

&nbsp;

### Other ARM devices:  
It would be perfect if you could help with other ARM devices. The scratch file should work on all ARM devices [archlinuxarm.org](https://archlinuxarm.org/platforms).
