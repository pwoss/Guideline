# **We moved everything to our own Gitea instance -> [git.pwoss.xyz](https://git.pwoss.xyz/PwOSS/Guideline)**

# PwOSS - Guideline

The PwOSS - Guideline is at https://guideline.pwoss.xyz.

If you find any typos, wrong explanation or if you have any other idea create an __issue__ or a __pull request__ on GitHub.

&nbsp;

## Info
More information about __PwOSS - Privacy with Open Source Software__ at https://pwoss.xyz/.

&nbsp;
&nbsp;

## License

<a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license"><img style="border-width: 0;" src="https://pwoss.xyz/wp-content/uploads/2018/07/licensebutton.png" alt="Creative Commons License" /></a>
